/****************************************************************************
 *
 * Copyright (c) 2014 - 2017 Samsung Electronics Co., Ltd. All rights reserved
 *
 *****************************************************************************/

#ifndef _SCSC_RELEASE_H
#define _SCSC_RELEASE_H

#define SCSC_RELEASE_PRODUCT 5
#define SCSC_RELEASE_ITERATION 23
#define SCSC_RELEASE_CANDIDATE 2


#endif


